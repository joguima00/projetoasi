import requests, json, time, platform, socket, os
from Client import disks, memory, network, cpu, expressions

hostname = socket.gethostname()
ip = socket.gethostbyname(hostname)

gettoken = {"os": platform.system(), "ip": ip}
token = ""

os.system('python ../Fake-Apache-Log-Generator/'
          'apache-fake-log-gen.py -n 100 -o LOG')

if not os.path.isfile("token.txt"):
    file = open("token.txt", 'w')

    res = requests.post('http://10.10.10.11:5000/token', json=gettoken)
    if res.ok:
        token = res.json()['token']
        print(res.json())
        file.writelines(token)
        file.close()
else:
    file = open("token.txt", 'r')
    token = file.readlines()[0]
    print("A sua token é: " + token)

print("A aguardar resposta do Servidor...")
getMenu = requests.get('http://10.10.10.10:5000/getMenu', json=token)
menu = getMenu.json()
print(menu)
data = {}
data["token"] = token
data["ip"] = ip
data["os"] = platform.system()
if menu["cpu"]:
    data["cpu"] = cpu.cpuInfo()
if menu["memory"]:
    data["memory"] = memory.memoryInfo()
if menu["disks"]:
    data["disks"] = disks.disksInfo()
if menu["network"]:
    data["network"] = network.network()
if menu["statusCode"]:
    data["statusCode"] = expressions.getStatusCode()

print(json.dumps(data, indent=4))

i = 0
while i < int(menu["manytimes"]):
    res = requests.post('http://10.10.10.10:5000/setData', json=data)
    if res.ok:
        print(res.json())
    i += 1
    time.sleep(int(menu["time"]))
