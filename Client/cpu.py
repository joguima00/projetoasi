import psutil


def numberCPUs():
    return psutil.cpu_count(logical=True)


def CPUload():
    return psutil.cpu_percent()


def cpuInfo():
    info = {}
    info["NumberCPUs"] = numberCPUs()
    info["CPULoad"] = CPUload()
    return info
