import psutil


def totalSize():
    info = {}
    partitions = psutil.disk_partitions()
    for partition in partitions:
        try:
            partition_usage = psutil.disk_usage(partition.mountpoint)
        except PermissionError:
            continue
        info['Disk ' + str(partition.mountpoint) + ' TotalSize '] = (partition_usage.total >> 20)
    return info


def used():
    info = {}
    partitions = psutil.disk_partitions()
    for partition in partitions:
        try:
            partition_usage = psutil.disk_usage(partition.mountpoint)
        except PermissionError:
            continue
        info['Disk ' + str(partition.mountpoint) + ' Used '] = (partition_usage.used >> 20)
    return info


def percentage():
    info = {}
    partitions = psutil.disk_partitions()
    for partition in partitions:
        try:
            partition_usage = psutil.disk_usage(partition.mountpoint)
        except PermissionError:
            continue
        info['Disk ' + str(partition.mountpoint) + ' Percentage '] = partition_usage.percent
    return info


def disksInfo():
    info = {}
    partitions = psutil.disk_partitions()
    for partition in partitions:
        try:
            partition_usage = psutil.disk_usage(partition.mountpoint)
        except PermissionError:
            continue
        info.update({'Disk' + str(partition.mountpoint).strip("\\\\") + 'TotalSize': (partition_usage.total >> 20),
                     'Disk' + str(partition.mountpoint).strip("\\\\") + 'Used': (partition_usage.used >> 20),
                     'Disk' + str(partition.mountpoint).strip("\\\\") + 'Percentage': partition_usage.percent})
    return info
