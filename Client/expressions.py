import os, re, glob


def getStatusCode():
    error200 = re.compile(r"^[^\"]*\"[^\"]*\sHTTP\/[\d.]+\"\s+(?:200)\s.*$")
    error4xx = re.compile(r"^[^\"]*\"[^\"]*\sHTTP\/[\d.]+\"\s+(?:4[0-9][0-9])\s.*$")
    error5xx = re.compile(r"^[^\"]*\"[^\"]*\sHTTP\/[\d.]+\"\s+(?:4[0-9][0-9])\s.*$")

    modulo = '..\Client'

    caminhos = [os.path.join(modulo, nome) for nome in os.listdir(modulo)]
    files = [arq for arq in caminhos if os.path.isfile(arq)]
    logs = [arq for arq in caminhos if arq.lower().endswith(".log")]

    list = {"numberErrors200": 0, "numberErrors4xx": 0, "numberErrors5xx": 0}

    for arq in logs:
        with open(arq) as file:
            for line in file:
                if error200.match(line):
                    list.update({"numberErrors200": list["numberErrors200"] + 1})
                if error4xx.match(line):
                    list.update({"numberErrors4xx": list["numberErrors4xx"] + 1})
                if error5xx.match(line):
                    list.update({"numberErrors5xx": list["numberErrors5xx"] + 1})

    return list
