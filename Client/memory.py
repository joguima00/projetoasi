import psutil


def totalMemory():
    return round(psutil.virtual_memory().total >> 20)


def UseMemory():
    return round(psutil.virtual_memory().used >> 20)


def memoryInfo():
    info = {}
    info["Totalmemory"] = totalMemory()
    info["Usememory"] = UseMemory()
    return info
