import socket


def network():
    info = {}
    ports = [135, 137, 138, 139, 140, 445]
    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)
    for port in ports:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = s.connect_ex((ip, port))

        if result == 0:
            info['Port' + str(port)] = "Active"
        else:
            info['Port' + str(port)] = "Inactive"
        s.close()
    return info
