## Contents

- [Purpose](#purpose)
- [Authors](#authors)

## Purpose

Este projeto foi realizado no âmbito da disciplina de Administração de Sistemas Informáticos da licenciatura de Engenharia Informática da Escola Superior de Tecnologia e Gestão do Politécnico do Porto. O projeto consiste na implementação de um sistema de monitorização
utilizando a linguagem de programação Python.


### Version Control

- [GitLab](https://gitlab.com/) - Version control


## Authors

- **João Guimarães** - 8180546





## HELP Install and run the project
- 1- Deve fazer o clone deste project - git clone https://gitlab.com/joguima00/projetoasi.git
- 2- Instalar todos os modulos necessários - executar este comando quando estiver dentro da pasta do porjeto pip3 install -r requirements.txt
- 3- [INFLUXDB DB INSTALL – DOCKER CONTAINER]
    - docker pull influxdb
    - mkdir -p /asi2020/influxdata
    - docker run -it -v /asi2020/influxdata:/var/lib/influxdb -p 8086:8086 --name influx -d influxdb 
- 4- [Abrir porto na FW para acesso ao WebService]
    - firewall-cmd --zone=public --permanent --add-port 8086/tcp
    - firewall-cmd --reload
- 5- [GRAFANA SERVER INSTALL – DOCKER CONTAINER]
    - docker run -d -p 3000:3000 --name grafana grafana/grafana:latest
    - firewall-cmd --zone=public --permanent --add-port 3000/tcp
    - firewall-cmd --reload 
- 6- executar o ficheiro webservice.py 
- 7- ver o ip que o sever dá e alterar no ficheiro client.py para os GET e POST
- 8- executar o ficheiro client.py
- 9- no run do servidor quando o client é executado será necessário uma ação por parte do servidor para saber o que vai monitorizar
- 10- irá ser enviado para o servidor a monitorização do client 
- 11- acesso via web ao grafana
    - http://10.10.10.10:3000/login
    - User: admin
    - Password: admin








