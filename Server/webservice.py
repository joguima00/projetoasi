import hashlib

from flask import Flask, request
from influxdb import InfluxDBClient
from datetime import datetime
import random

components = {"cpu": True, "memory": True, "disks": True, "network": True, "manytimes": 0, "time": 0,
              "statusCode": True}


def componentMenu(comp):
    inpu = True
    while inpu:
        print("Fazer a monitorização " + comp + " (sim/nao)")
        t = input()
        if t == "nao":
            inpu = False
            components[comp] = False
        else:
            if t == "sim":
                inpu = False
                components[comp] = True


def menu():
    componentMenu("cpu")
    componentMenu("memory")
    componentMenu("disks")
    componentMenu("network")
    componentMenu("statusCode")

    print("Quantas vezes deseja fazer a monitorização?")
    components["manytimes"] = input()
    print("De quanto em quanto tem deseja fazer?")
    components["time"] = input()
    return components


app = Flask(__name__)


def check_db():
    myclient = InfluxDBClient(host='10.10.10.10', port=8086)

    dblist = myclient.get_list_database()
    if "machinedata" not in dblist:
        myclient.create_database('machinedata')
    return myclient


def store_DB(myclient, dados):
    myclient.switch_database('machinedata')
    res = myclient.write_points(dados)
    return (res)


def get_DB(myclient):
    myclient.switch_database('machinedata')
    result = myclient.query('SELECT * FROM machinedata')
    return (result)


@app.route('/setData', methods=['POST'])
def setData():
    myclient = check_db()
    req_data = request.get_json()
    ins = {}
    ins['measurement'] = "machinedata"
    ins["tags"] = {"token": req_data.get("token"),
                   "ip": req_data.get("ip"),
                   "os": req_data.get("os")}
    ins["time"] = datetime.now()
    ins["fields"] = {}

    if "cpu" in req_data.keys():
        ins["fields"].update(req_data.get("cpu"))
    if "memory" in req_data.keys():
        ins["fields"].update(req_data.get("memory"))
    if "disks" in req_data.keys():
        ins["fields"].update(req_data.get("disks"))
    if "network" in req_data.keys():
        ins["fields"].update(req_data.get("network"))
    if "statusCode" in req_data.keys():
        ins["fields"].update(req_data.get("statusCode"))
    json_body = [ins]
    res = store_DB(myclient, json_body)

    print(get_DB(myclient))
    return {"status": str(res)}


@app.route('/getMenu', methods=['GET'])
def getMenu():
    req_data = request.get_json()
    print("O Cliente que vai fazer a monitorização é: " + req_data)
    c = menu()
    return c


@app.route('/token', methods=['POST'])
def token():
    req_data = request.get_json()
    os = req_data['os']
    ip = req_data['ip']

    value = random.randint(0, 99999999)
    client = os + ip + str(value)
    hash_token = hashlib.sha512(str(client).encode("utf-8")).hexdigest()

    return {"token": hash_token}


if __name__ == '__main__':
    app.run(host="10.10.10.10")
